package com.game;

import com.game.entities.COLOR;
import com.game.entities.GameEntity;
import com.game.entities.MoveEntity;
import com.game.entities.PlayerEntity;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.*;
import java.util.stream.Collectors;


public class RequestHandler {
    private Map<String, GameEntity> gameArchive = new HashMap<>();
    private Map<String, PlayerEntity> allPlayers = new HashMap<>();
    private List<PlayerEntity> waitingPlayers = new ArrayList<>();
    private static RequestHandler requestHandlerObject = new RequestHandler();

    private RequestHandler() {

    }

    public static RequestHandler getObject() {
        return requestHandlerObject;
    }

    public boolean storeMove(JSONObject moveObject) throws JSONException {

        if (!moveObject.has(MoveEntity.GAME_NUMBER) || gameArchive.get(moveObject.get(MoveEntity.GAME_NUMBER)) == null) {
            return false;
        }
        return gameArchive.get(moveObject.get(MoveEntity.GAME_NUMBER)).storeMove(moveObject);
    }

    public JSONObject getMove(String gameNumber, int moveNumber) {
        if (gameArchive.get(gameNumber) == null) {
            return null;
        }
        return gameArchive.get(gameNumber).getMove(moveNumber);
    }

    public void registerPlayer(String sessionNumber, String playerName) {
        PlayerEntity player = new PlayerEntity(null, sessionNumber, playerName);
        if (!this.waitingPlayers.contains(player)) {
            waitingPlayers.add(player);
        }
        if (!this.allPlayers.keySet().contains(player.getSessionNum())) {
            allPlayers.put(player.getSessionNum(), player);
        }
    }

    public JSONObject matchPlayers(String sessionA, String sessionB) throws JSONException {
        PlayerEntity playerA = getPlayerForSession(sessionA);
        PlayerEntity playerB = getPlayerForSession(sessionB);
        JSONObject returnObject = new JSONObject();
        if (playerA != null || playerB != null) {
            returnObject.put("result", "failure");
        }
        playerA.getLock().lock();
        playerB.getLock().lock();
        waitingPlayers.remove(playerA);
        waitingPlayers.remove(playerB);
        String gameNumber = UUID.randomUUID().toString();
        GameEntity game = new GameEntity(gameNumber, playerA, playerB);
        gameArchive.put(gameNumber, game);
        playerA.setGame(game);
        playerB.setGame(game);
        playerA.getLock().unlock();
        playerB.getLock().unlock();
        returnObject.put("result", "success");
        returnObject.put("gameNumber", gameNumber);
        returnObject.put("redPlayer", playerA.getPlayerJsonObject());
        returnObject.put("bluePlayer", playerB.getPlayerJsonObject());
        return returnObject;
    }

    private PlayerEntity getPlayerForSession(String sessionNum) {
        for (PlayerEntity player : waitingPlayers) {
            if (player.getSessionNum().equals(sessionNum)) {
                return player;
            }
        }
        return null;
    }

    public JSONObject getWaitingPlayersOrMatchedOpponent(String sessionNumber) throws JSONException {
        PlayerEntity requestingPlayer = allPlayers.get(sessionNumber);
        JSONObject returnedObject = new JSONObject();
        GameEntity matchedGame = (requestingPlayer == null ? null : requestingPlayer.getGame());
        if (matchedGame == null) {
            returnedObject.put("assignedToGame", "false");
            List<JSONObject> waitingPlayers = this.waitingPlayers.stream().map( player -> {
                try {
                    if (player.getSessionNum() == sessionNumber) {
                        return null;
                    }
                    return player.getPlayerJsonObject();
                } catch (JSONException e) {
                    e.printStackTrace();
                    return null;
                }
            }).collect(Collectors.toList());
            waitingPlayers = waitingPlayers.stream().filter(player -> player != null).collect(Collectors.toList());
            returnedObject.put("players", waitingPlayers);
        }
        else {
            // there is a match
            returnedObject.put("assignedToGame", "true");
            returnedObject.put("gameNumber", matchedGame.getGameNumber());
            if (!matchedGame.getBluePlayer().getSessionNum().equals(sessionNumber)) {
                // opponent is blue
                PlayerEntity bluePlayer = matchedGame.getBluePlayer();
                bluePlayer.setColor(COLOR.BLUE);
                returnedObject.put("bluePlayer", matchedGame.getBluePlayer().getPlayerJsonObject());
            }
            else {
                // opponent is red
                PlayerEntity redPlayer = matchedGame.getRedPlayer();
                redPlayer.setColor(COLOR.RED);
                returnedObject.put("redPlayer", matchedGame.getRedPlayer().getPlayerJsonObject());
            }
        }
        return returnedObject;
    }

}
