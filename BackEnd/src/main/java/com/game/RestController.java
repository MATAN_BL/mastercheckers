package com.game;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;


@Controller
public class RestController {

    private static RequestHandler requestHandler = RequestHandler.getObject();

    @RequestMapping("/")
    public String getNewGame () {
        return "index.html";
    }

    @RequestMapping(value = "/writeToServer", method = RequestMethod.POST)
    @ResponseBody
    public String writeToServer(@RequestBody String payload) throws JSONException {
        JSONObject moveObject =  new JSONObject(payload);
        requestHandler.storeMove(moveObject);
        return payload;
    }

    @RequestMapping("/readFromServer")
    @ResponseBody
    public String readFromServer(@RequestParam(value="gameNumber") String gameNumber,
                                 @RequestParam(value="moveNumber") String moveNumber) {
        JSONObject moveObject = requestHandler.getMove(gameNumber, Integer.parseInt(moveNumber));
        return (moveObject == null ? null : moveObject.toString());
    }

    @RequestMapping(value = "/registerPlayer", method = RequestMethod.POST)
    @ResponseBody
    public void registerPlayer(@RequestBody String payload) throws JSONException {
        JSONObject registrationObject =  new JSONObject(payload);
        if (registrationObject.has("playerName") && !registrationObject.get("playerName").toString().equals("null") &&
                registrationObject.has("sessionNumber") && !registrationObject.get("sessionNumber").toString().equals("null")) {
            requestHandler.registerPlayer(registrationObject.getString("sessionNumber"),
                    registrationObject.getString("playerName"));
        }
    }

    @RequestMapping(value = "/matchPlayers", method = RequestMethod.POST)
    @ResponseBody
    public String matchPlayers(@RequestBody String payload) throws JSONException {
        JSONObject matchObject =  new JSONObject(payload);
        if (matchObject.has("sessions") && matchObject.get("sessions").getClass() ==  org.json.JSONArray.class &&
                ((JSONArray) matchObject.get("sessions")).length() == 2) {
            String sessionA = ((JSONArray) matchObject.get("sessions")).get(0).toString();
            String sessionB = ((JSONArray) matchObject.get("sessions")).get(1).toString();
            if (sessionA.hashCode() < sessionB.hashCode()) {
                return requestHandler.matchPlayers(sessionA, sessionB).toString();
            }
            else {
                return requestHandler.matchPlayers(sessionB, sessionA).toString();
            }
        }
        else {
            matchObject.put("result", "failure");
        }
        return matchObject.toString();
    }

    @RequestMapping(value = "/getWaitingPlayersOrMatchedOpponent", method = RequestMethod.POST)
    @ResponseBody
    public String getWaitingPlayersOrMatchedOpponent(@RequestBody String payload) throws JSONException {
        return requestHandler.getWaitingPlayersOrMatchedOpponent((String) (new JSONObject(payload)).get("sessionNumber"))
                .toString();
    }
}
