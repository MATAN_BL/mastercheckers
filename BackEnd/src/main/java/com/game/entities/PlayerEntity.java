package com.game.entities;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class PlayerEntity {

    private String ipAddress;
    private String sessionNumber;
    private String name;
    private Lock lock = new ReentrantLock();
    // will be set to COLOR.RED or COLOR.BLUE when the player is assigned to game.
    private COLOR color = null;
    private GameEntity game = null;

    public PlayerEntity(String ipAddress, String sessionNumber, String name) {
        this.ipAddress = ipAddress;
        this.sessionNumber = sessionNumber;
        this.name = name;
    }

    public JSONObject getPlayerJsonObject() throws JSONException {
        JSONObject playerObject = new JSONObject();
        playerObject.put("ipAddress", ipAddress);
        playerObject.put("sessionNumber", sessionNumber);
        playerObject.put("playerName", name);
        assert(color == COLOR.RED || color == COLOR.BLUE);
        playerObject.put("color", color == COLOR.RED ? "RED" : "BLUE");
        return playerObject;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public String getSessionNum() {
        return sessionNumber;
    }

    public String getName() {
        return name;
    }

    public Lock getLock() {
        return lock;
    }

    public void setLock(Lock lock) {
        this.lock = lock;
    }

    public GameEntity getGame() {
        return game;
    }

    public void setGame(GameEntity game) {
        this.game = game;
    }

    public COLOR getColor() {
        return color;
    }

    public void setColor(COLOR color) {
        this.color = color;
    }

    @Override
    public int hashCode() {
        return sessionNumber.hashCode();
    }

    @Override
    public boolean equals(Object otherPlayerEntity) {
        if ((otherPlayerEntity instanceof PlayerEntity) && (otherPlayerEntity.hashCode() == hashCode())) {
            return true;
        }
        return false;
    }

}
