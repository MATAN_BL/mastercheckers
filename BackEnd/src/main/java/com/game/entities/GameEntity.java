package com.game.entities;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class GameEntity {

    private String gameNumber;
    private PlayerEntity redPlayer;
    private PlayerEntity bluePlayer;
    private Map<Integer, JSONObject> movesArchive = new HashMap<>();

    public GameEntity(String gameNumber, PlayerEntity redPlayer, PlayerEntity bluePlayer) {
        this.gameNumber = gameNumber;
        this.redPlayer = redPlayer;
        redPlayer.setColor(COLOR.RED);
        this.bluePlayer = bluePlayer;
        bluePlayer.setColor(COLOR.BLUE);
    }

    public boolean storeMove(JSONObject moveObject) throws JSONException {
        if (!moveObject.has(MoveEntity.MOVE_NUMBER)) {
            return false;
        }
        String moveNumber = moveObject.getString(MoveEntity.MOVE_NUMBER);
        if (movesArchive.keySet().size() + 1 == Integer.valueOf(moveNumber)) {
            movesArchive.put(moveObject.getInt(MoveEntity.MOVE_NUMBER), moveObject);
            return true;
        }
        return false;
    }

    public JSONObject getMove(int moveNumber) {
        return movesArchive.get(moveNumber);
    }

    public JSONObject getWaitingPlayer() throws JSONException {
        if (redPlayer == null && bluePlayer != null) {
            return bluePlayer.getPlayerJsonObject();
        }
        if (redPlayer != null && bluePlayer == null) {
            return redPlayer.getPlayerJsonObject();
        }
        return null;
    }

    public String getGameNumber() {
        return gameNumber;
    }

    public void setGameNumber(String gameNumber) {
        this.gameNumber = gameNumber;
    }

    public PlayerEntity getRedPlayer() {
        return redPlayer;
    }

    public void setRedPlayer(PlayerEntity redPlayer) {
        this.redPlayer = redPlayer;
    }

    public PlayerEntity getBluePlayer() {
        return bluePlayer;
    }

    public void setBluePlayer(PlayerEntity bluePlayer) {
        this.bluePlayer = bluePlayer;
    }

    public Map<Integer, JSONObject> getMovesArchive() {
        return movesArchive;
    }

    public void setMovesArchive(Map<Integer, JSONObject> movesArchive) {
        this.movesArchive = movesArchive;
    }
}
