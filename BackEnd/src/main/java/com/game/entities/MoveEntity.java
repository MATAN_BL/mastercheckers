package com.game.entities;

import javafx.util.Pair;
import java.util.List;

public class MoveEntity {

    public static final String MOVE_NUMBER = "moveNumber";
    public static final String GAME_NUMBER = "gameNumber";

    private String gameNumber;
    private int moveNumber;
    private Pair<Integer, Integer> fromClick;
    private Pair<Integer, Integer> toClick;
    private List<Pair<Integer, Integer>> markedSquares;
    private List<Pair<Integer, Integer>> pawnsToRemove;

}
