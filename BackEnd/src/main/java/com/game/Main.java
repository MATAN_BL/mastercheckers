package com.game;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.File;
import java.util.Arrays;
import java.util.Properties;

@SpringBootApplication
@EnableAutoConfiguration
public class Main {
    public static void main(String[] args) {
        SpringApplication app = new SpringApplication(Main.class);
        Properties properties = new Properties();
        properties.setProperty("spring.resources.static-locations",
                getPathForStaticFiles());
        app.setDefaultProperties(properties);
        app.run(args);
    }

    private static String getPathForStaticFiles() {
        String[] pathToRoot;
        String pathToParentFolder;
        if (System.getProperty("os.name").contains("Windows")) {
            pathToRoot = new File("").getAbsolutePath().split("\\\\");
            pathToParentFolder = String.join("\\", Arrays.copyOfRange(pathToRoot, 0, pathToRoot.length - 1));
            return "file:" + pathToParentFolder + "\\FrontEnd\\build";
        }
        pathToRoot = new File("").getAbsolutePath().split("/");
        pathToParentFolder = String.join("/", Arrays.copyOfRange(pathToRoot, 0, pathToRoot.length - 1));
        return "file:" + pathToParentFolder + "/FrontEnd/build";
    }

}


