import React from 'react';
import ReactDOM from 'react-dom';
import Game from './Components/Game/Game';
import registerServiceWorker from './registerServiceWorker';
import Landing from "./Components/Landing/Landing";
import { BrowserRouter, Switch, Route } from 'react-router-dom'
import { SESSION_STORAGE_ITEM_NAME } from './Utilities/Enum';

sessionStorage.removeItem(SESSION_STORAGE_ITEM_NAME);

ReactDOM.render((
    <div>
        <BrowserRouter>
            <Switch>
                <Route exact path='/' component={Landing} />
                <Route exact path='/one-session-game' render={() => {
                    let props = {pawnsIndex: 0, oneSession: true};
                    return <Game {...props}/>
                }}/>

                <Route exact path='/two-session-game' render={() => {
                    let props = {pawnsIndex: 0, oneSession: false};
                    return <Game {...props}/>
                }} />
            </Switch>
        </BrowserRouter>
    </div>
), document.getElementById('root'));

registerServiceWorker();
