import Utils from '../Utilities/BoardUtils.js';
import {COLOR} from '../Utilities/Enum.js';
import '../Components/Game/Game.scss';
import '../Components/Game/Square.scss'

test('test method opponentTeam', () => {
    let app = {state: {turn: COLOR.BLUE }};
    let utils = new Utils(app);
    expect(utils.opponentTeam()).toBe(COLOR.RED);

    app = {state: {turn: COLOR.RED }};
    utils = new Utils(app);
    expect(utils.opponentTeam()).toBe(COLOR.BLUE);
});