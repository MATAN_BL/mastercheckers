import MessageBox from "../Components/Game/MessageBox";
import { shallow } from 'enzyme';
import React from 'react';
import "../setupTests"

test('message box', () => {
   const game = {setState: jest.fn(), state: {modalIsOpen: true}};
   const wrapper = shallow(<MessageBox game={game}/>);
   wrapper.find('button').at(0).simulate('click');
   expect(game.setState).toHaveBeenLastCalledWith({modalIsOpen: false});
});