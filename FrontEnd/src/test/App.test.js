import Game from "../Components/Game/Game";
import { shallow } from 'enzyme';
import React from 'react';
import {SUSPENSION_TIME, COLOR} from "../Utilities/Enum";
import "../setupTests"
import RegularPawn from "../Components/Game/Pawns/RegularPawn";

jest.useFakeTimers();
jest.mock('../Utilities/Network.js');

test('move a pawn from {row:5, col:2} to {row:4 col:3}', () => {
    const wrapper = shallow(<Game pawnsIndex={0} oneSession={true}/>);
    //Stage 1: check that it's RED turn
    expect(wrapper.state().turn).toBe(COLOR.RED);

    // Stage 2: click the squares and check that squares have been marked
    wrapper.findWhere(node => node.key() === "5,2").simulate('click');
    expect(wrapper.state().markedSquares).toEqual([{row: 5, col: 2}]);
    wrapper.findWhere(node => node.key() === "4,3").simulate('click');
    expect(wrapper.state().markedSquares).toEqual([{row: 5, col: 2}, {row: 4, col: 3}]);

    // Stage 3: check that squares stay marked until SUSPENSION_TIME expires
    jest.runTimersToTime(SUSPENSION_TIME - 1);
    expect(wrapper.state().markedSquares).toEqual([{row: 5, col: 2}, {row: 4, col: 3}]);

    // Stage 4: check that squares are not marked once SUSPENSION_TIME expires
    jest.runTimersToTime(1);
    expect(wrapper.state().markedSquares).toEqual([]);

    // Stage 5: check that the pawn has changed their places
    expect(wrapper.state().pawns[5][2]).toBe(null);
    expect(wrapper.state().pawns[4][3].color).toBe(COLOR.RED);
    expect(wrapper.state().pawns[4][3] instanceof RegularPawn).toBe(true);

    // Stage 6: check that it's BLUE turn
    expect(wrapper.state().turn).toBe(COLOR.BLUE);
})

test('double strike by pawn located at {row:5, col:2}', () => {
    const wrapper = shallow(<Game pawnsIndex={3} oneSession={true}/>);
    // Stage 1: click for double strike
    wrapper.findWhere(node => node.key() === "5,4").simulate('click');
    expect(wrapper.state().markedSquares).toEqual([{row: 5, col: 4}]);
    wrapper.findWhere(node => node.key() === "3,2").simulate('click');
    expect(wrapper.state().markedSquares).toEqual([{row: 5, col: 4}, {row: 3, col: 2}]);
    wrapper.findWhere(node => node.key() === "1,0").simulate('click');
    expect(wrapper.state().markedSquares).toEqual([{row: 5, col: 4}, {row: 3, col: 2}, {row: 1, col: 0}]);

    // Stage 2: check pawns setup
    jest.runTimersToTime(SUSPENSION_TIME);
    expect(wrapper.state().pawns[5][4]).toBe(null);
    expect(wrapper.state().pawns[4][3]).toBe(null);
    expect(wrapper.state().pawns[2][1]).toBe(null);
    expect(wrapper.state().pawns[1][0].color).toBe(COLOR.RED);
    expect(wrapper.state().pawns[1][0] instanceof RegularPawn).toBe(true);

    // Stage 3: check number of remaining pawns
    expect(wrapper.instance().numberOfPawns[COLOR.RED]).toBe(12);
    expect(wrapper.instance().numberOfPawns[COLOR.BLUE]).toBe(10);

    // Stage 4: check that it's BLUE turn
    expect(wrapper.state().turn).toBe(COLOR.BLUE);
});

test('check ESC button', () => {
    const wrapper = shallow(<Game pawnsIndex={0} oneSession={true}/>);

    // Stage 1: click a pawn and check that application behaves properly
    wrapper.findWhere(node => node.key() === "5,2").simulate('click');
    expect(wrapper.state().markedSquares).toEqual([{row: 5, col: 2}]);
    expect(wrapper.instance().playingPawn).toBe(wrapper.instance().state.pawns[5][2]);

    // Stage 2: click ESC button and check that data is cleaned
    wrapper.instance().escFunction({keyCode: 27});
    expect(wrapper.state().markedSquares).toEqual([]);
    expect(wrapper.instance().playingPawn).toBe(null);
});

test('triple chain move', () => {
    const wrapper = shallow(<Game pawnsIndex={5} oneSession={true}/>);

    // Stage 1: pick a pawn
    wrapper.findWhere(node => node.key() === "6,7").simulate('click');
    expect(wrapper.state().markedSquares).toEqual([{row: 6, col: 7}]);
    expect(wrapper.instance().pawns[6][7].getClassName()).toEqual("redPawn");

    // Stage 2: first strike
    jest.runTimersToTime(SUSPENSION_TIME - 1);
    wrapper.findWhere(node => node.key() === "4,5").simulate('click');
    expect(wrapper.state().markedSquares).toEqual([{row: 6, col: 7}, {row: 4, col: 5}]);
    expect(wrapper.instance().pawns[6][7].getClassName()).toEqual("redPawn");
    expect(wrapper.instance().pawns[5][6].getClassName()).toEqual("bluePawn");
    expect(wrapper.instance().pawns[4][5]).toEqual(null);


    //Stage 3: second strike
    jest.runTimersToTime(SUSPENSION_TIME - 1);
    wrapper.findWhere(node => node.key() === "2,3").simulate('click');
    expect(wrapper.state().markedSquares).toEqual([{row: 6, col: 7}, {row: 4, col: 5}, {row: 2, col: 3}]);
    expect(wrapper.instance().pawns[6][7].getClassName()).toEqual("redPawn");
    expect(wrapper.instance().pawns[5][6].getClassName()).toEqual("bluePawn");
    expect(wrapper.instance().pawns[4][5]).toEqual(null);
    expect(wrapper.instance().pawns[3][4].getClassName()).toEqual("bluePawn");
    expect(wrapper.instance().pawns[2][3]).toEqual(null);

    //Stage 4: third strike
    jest.runTimersToTime(SUSPENSION_TIME - 1);
    wrapper.findWhere(node => node.key() === "0,1").simulate('click');
    expect(wrapper.state().markedSquares).toEqual([{row: 6, col: 7}, {row: 4, col: 5}, {row: 2, col: 3}, {row: 0, col: 1}]);
    expect(wrapper.instance().pawns[6][7].getClassName()).toEqual("redPawn");
    expect(wrapper.instance().pawns[5][6].getClassName()).toEqual("bluePawn");
    expect(wrapper.instance().pawns[4][5]).toEqual(null);
    expect(wrapper.instance().pawns[3][4].getClassName()).toEqual("bluePawn");
    expect(wrapper.instance().pawns[2][3]).toEqual(null);
    expect(wrapper.instance().pawns[1][2].getClassName()).toEqual("bluePawn");
    expect(wrapper.instance().pawns[0][1]).toEqual(null);

    //Stage 5: make the strike happen
    jest.runTimersToTime(SUSPENSION_TIME + 1);
    expect(wrapper.state().markedSquares).toEqual([]);
    expect(wrapper.instance().pawns[6][7]).toEqual(null);
    expect(wrapper.instance().pawns[5][6]).toEqual(null);
    expect(wrapper.instance().pawns[4][5]).toEqual(null);
    expect(wrapper.instance().pawns[3][4]).toEqual(null);
    expect(wrapper.instance().pawns[2][3]).toEqual(null);
    expect(wrapper.instance().pawns[1][2]).toEqual(null);
    expect(wrapper.instance().pawns[0][1].getClassName()).toEqual("redMasterPawn");

    //Stage 6: check victory
    expect(wrapper.state().modalIsOpen).toEqual(true);
    expect(wrapper.state().modalMessage).toEqual("RED wins!");
})