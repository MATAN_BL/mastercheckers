/**
 * Created by matan on 13/08/18.
 */

import {COLOR} from './Enum.js';

export function assert(condition, message) {
    if (condition === false) {
        throw new Error(message);
    }
}

export function getOppositeColor(color) {
    assert(color === COLOR.BLUE || color === COLOR.RED, "input color can be RED or BLUE");
    return (color === COLOR.RED ? COLOR.BLUE : COLOR.RED);
}