import {COLOR} from './Enum.js';
import RegularPawn from "../Components/Game/Pawns/RegularPawn.js";
import MasterPawn from "../Components/Game/Pawns/MasterPawn.js";


export default class BoardUtils {
    constructor(app) {
        this.app = app;
    }
    initBoard(index) {
        switch (index) {
            case 0:
                return this.initBoard0();
            case 1:
                return this.initBoard1();
            case 2:
                return this.initBoard2();
            case 3:
                return this.initBoard3();
            case 4:
                return this.initBoard4();
            case 5:
                return this.initBoard5();
        }
    }
    initBoard0() {
        return [[null, this.blueRegularPawn(), null, this.blueRegularPawn(), null, this.blueRegularPawn(), null, this.blueRegularPawn()],
            [this.blueRegularPawn(), null, this.blueRegularPawn(), null, this.blueRegularPawn(), null, this.blueRegularPawn(), null],
            [null, this.blueRegularPawn(), null, this.blueRegularPawn(), null, this.blueRegularPawn(), null, this.blueRegularPawn()],
             Array(8).fill(null), Array(8).fill(null),
            [this.redRegularPawn(), null, this.redRegularPawn(), null, this.redRegularPawn(), null, this.redRegularPawn()],
            [null, this.redRegularPawn(), null, this.redRegularPawn(), null, this.redRegularPawn(), null, this.redRegularPawn()],
            [this.redRegularPawn(), null, this.redRegularPawn(), null, this.redRegularPawn(), null, this.redRegularPawn()]];
    }
    initBoard1() {
        return [[null, this.blueRegularPawn(), null, this.blueRegularPawn(), null, this.blueRegularPawn(), null, this.blueRegularPawn()],
            [this.blueMasterPawn(), null, this.blueRegularPawn(), null, this.blueRegularPawn(), null, this.blueRegularPawn(), null],
            Array(8).fill(null), Array(8).fill(null), Array(8).fill(null), Array(8).fill(null),
            [null, this.redRegularPawn(), null, this.redRegularPawn(), null, this.redRegularPawn(), null, this.redRegularPawn()],
            [this.redRegularPawn(), null, this.redRegularPawn(), null, this.redRegularPawn(), null, this.redRegularPawn()]];
    }

    initBoard2() {
        return [[null, null, null, null, null, this.blueRegularPawn(), null, this.blueRegularPawn()],
            [this.blueRegularPawn(), null, this.redRegularPawn(), null, this.blueRegularPawn(), null, this.blueMasterPawn(), null],
            Array(8).fill(null), Array(8).fill(null), Array(8).fill(null), Array(8).fill(null),
            [null, this.redRegularPawn(), null, this.redRegularPawn(), null, this.redRegularPawn(), null, this.redRegularPawn()],
            [this.redRegularPawn(), null, this.redRegularPawn(), null, this.redRegularPawn(), null, null, null]];
    }

    initBoard3() {
        return [[null, this.blueRegularPawn(), null, this.blueRegularPawn(), null, this.blueRegularPawn(), null, this.blueRegularPawn()],
            [null, null, this.blueRegularPawn(), null, this.blueRegularPawn(), null, this.blueRegularPawn(), null],
            [null, this.blueRegularPawn(), null, this.blueRegularPawn(), null, this.blueRegularPawn(), null, this.blueRegularPawn()],
            Array(8).fill(null),
            [null, null, null, this.blueRegularPawn(), null, null, null, null],
            [this.redRegularPawn(), null, this.redRegularPawn(), null, this.redRegularPawn(), null, this.redRegularPawn(), null],
            [null, this.redRegularPawn(), null, this.redRegularPawn(), null, this.redRegularPawn(), null, this.redRegularPawn()],
            [this.redRegularPawn(), null, this.redRegularPawn(), null, this.redRegularPawn(), null, this.redRegularPawn()]];
    }

    initBoard4() {
        return [[null, null, null, null, null, null, null, null],
            [null, null, this.blueRegularPawn(), null, null, null, null, null],
            [null, null, null, this.redRegularPawn(), null, null, null, null],
            Array(8).fill(null),
            Array(8).fill(null),
            Array(8).fill(null),
            Array(8).fill(null),
            Array(8).fill(null)
        ];
    }

    initBoard5() {
        return [[null, null, null, null, null, null, null, null],
            [null, null, this.blueRegularPawn(), null, null, null, null, null],
            Array(8).fill(null),
            [null, null, null, null, this.blueRegularPawn(), null, null, null],
            Array(8).fill(null),
            [null, null, null, null, null, null, this.blueRegularPawn(), null],
            [null, null, null, null, null, null, null, this.redRegularPawn()],
            Array(8).fill(null)
        ];
    }
    
    blueRegularPawn() {
        return new RegularPawn(this.app, COLOR.BLUE);
    }

    redRegularPawn() {
        return new RegularPawn(this.app, COLOR.RED);
    }


    redMasterPawn() {
        return new MasterPawn(this.app, COLOR.RED);
    }

    blueMasterPawn() {
        return new MasterPawn(this.app, COLOR.BLUE);
    }

    opponentTeam() {
        if (this.app.state.turn === COLOR.RED) {
            return COLOR.BLUE;
        }
        else {
            return COLOR.RED;
        }
    }

    getNumberOfPawnsOfColor(pawns, color) {
        let counter = 0;
        for (let row of pawns) {
            for (let pawn of row) {
                if (pawn && pawn.color === color) {
                    counter++;
                }
            }
        }
        return counter;
    }
}

