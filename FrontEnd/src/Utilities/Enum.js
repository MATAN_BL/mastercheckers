export let CLICK_TYPE = {FROM: 'from', TO: 'to'};
export let DIRECTION = {BLUE: 1, RED: -1};
export let COLOR = {BLUE: 'BLUE', RED: 'RED'};
export let SUSPENSION_TIME = 1000; // ms
export let SESSION_STORAGE_ITEM_NAME = "masterCheckers";
export let SERVER_ADDRESS = "http://127.0.0.1:8080";