import { COLOR, SUSPENSION_TIME, SERVER_ADDRESS} from "./Enum";
import {getOppositeColor} from "./Helper.js";
/*
 The schema of move object we send/receive to/from server is:
 {
    gameNumber: String
    moveNumber: number,
    fromClick: [number, number],
    toClick: [number, number],
    markedSquares: Array[[number, number]]
    pawnsToRemove: Array[[number, number]]
 }
*/
export function readFromServer() {
    if (this.gameNumber === null || this.gameNumber === undefined) {
        return;
    }
    let _this = this;
    fetch(`${SERVER_ADDRESS}/readFromServer?gameNumber=${_this.gameNumber}&moveNumber=${_this.moveNumber}` , {
            method: 'GET',
        }, 1000)
        .then(res => {
            return res.body.getReader().read()
        })
        .then(data => {
            if (data === undefined || data.value === undefined) {
                return;
            }
            let moveFromServer = JSON.parse(convertAsciToChars(data.value));
            if (moveFromServer.moveNumber !== this.moveNumber) {
                return
            }
            this.moveNumber++;
            _this.setState({markedSquares: moveFromServer.markedSquares});
            setTimeout(() => {
                _this.movePawn(moveFromServer.fromClick, moveFromServer.toClick);
                moveFromServer.pawnsToRemove.forEach((pawn) => {
                    _this.removePawn(pawn.row, pawn.col);
                });
                _this.setState({markedSquares: []});
                _this.setState((prevState) => {
                    return {turn: getOppositeColor(prevState.turn)}
                });
            }, SUSPENSION_TIME);
        })
}


export function registerPlayer(sessionNumber, playerName) {
    return fetch(`${SERVER_ADDRESS}/registerPlayer` , {
        method: 'POST',
        body: JSON.stringify({
            sessionNumber, playerName
        })
    }, 1000)
        .then(data => {
            return data.body.getReader().read();
        }).then(data => {
            if (data === undefined || data.value === undefined) {
                return;
            }
            return JSON.parse(convertAsciToChars(data.value))['COLOR'];
        });
}

export function writeToServer(moveObject) {
    fetch(`${SERVER_ADDRESS}/writeToServer` , {
            method: 'POST',
            body: JSON.stringify(moveObject)
        }, 1000)
        .then(data => {
            ;
        })
}

export function connectToRemotePlayer(currentAddress, remoteAddress) {
    return fetch(`${SERVER_ADDRESS}/matchPlayers` , {
        method: 'POST',
        body: JSON.stringify({
            sessions: [currentAddress, remoteAddress]
        })
    }, 1000)
        .then(data => {
            return data.body.getReader().read();
        }).then(data => {
            if (data === undefined || data.value === undefined) {
                return;
            }
            return JSON.parse(convertAsciToChars(data.value));
        });
}

export function getWaitingPlayersOrMatchedOpponent(sessionNumber) {
    return fetch(`${SERVER_ADDRESS}/getWaitingPlayersOrMatchedOpponent` , {
        method: 'POST',
        body: JSON.stringify({sessionNumber: sessionNumber})
    }, 1000)
        .then(data => {
            return data.body.getReader().read();
        }).then(data => {
        return JSON.parse(convertAsciToChars(data.value));
    })
}

function convertAsciToChars(asciArray) {
    let retArray = [];
    for (let c of asciArray) {
        retArray.push(String.fromCharCode(c));
    }
    return retArray.join("");
}

export function getGameNumberFromServer() {
    try {
        if (window.location.search.includes("gameNumber=") ) {
            let queryString = window.location.search.substr(1).split('&');
            return queryString.filter((query) => {
                return query.includes("gameNumber")
            })[0].split("=")[1];
        }
        else {
            return generateUUID();
        }
    } catch(err) {
        console.log(err);
        return null;
    }
}

export function generateUUID() {
    let d = new Date().getTime();
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        let r = (d + Math.random()*16)%16 | 0;
        d = Math.floor(d/16);
        return (c === 'x' ? r : (r&0x3|0x8)).toString(16);
    });
}

