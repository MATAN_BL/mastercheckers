import React, { Component } from 'react';
import styles from './Landing.css';
import NameBox from './NameBox';
import JoinGameBox from './ChooseOpponentBox';
import {withRouter} from 'react-router-dom';


class Landing extends Component {

    constructor(props) {
        super(props);
        this.state = {
            nameBoxIsOpen: false,
            chooseOpponentBoxIsOpen: false,
            currentPlayer : {
                playerName: undefined,
                sessionNumber: undefined
            }
        };
    }

    getTwoSessionGame(joinExistingGame) {
        this.setState({nameBoxIsOpen: true});
    }

    moveToRoute(path) {
        this.props.history.push(path);
    }

    render() {
        return (
            <div className={styles.outer}>
                <div className={styles.inner}>
                    <h1 className={styles.title}> Welcome to Master Checkers! </h1>
                        <button onClick ={this.moveToRoute.bind(this, '/one-session-game')} className={styles.button}>
                            One Session Game
                        </button>
                        <button className={styles.button} onClick={this.getTwoSessionGame.bind(this)}>
                            Two Session Game
                        </button>
                    <NameBox landingComp={this} />
                    <JoinGameBox landingComp={this} />
                </div>
            </div>)
    }
}

export default withRouter(Landing);