import React, { Component } from 'react';
import Modal from 'react-modal';
import { withRouter} from 'react-router-dom';
import {getWaitingPlayersOrMatchedOpponent, connectToRemotePlayer} from "../../Utilities/Network";
import { SESSION_STORAGE_ITEM_NAME, SUSPENSION_TIME, COLOR } from '../../Utilities/Enum';
import { getOppositeColor } from '../../Utilities/Helper.js'
import styles from "./ChooseOpponentBox.css";

class ChooseOpponentBox extends Component {
    constructor(props) {
        super(props);
        this.landingComp = this.props.landingComp;
        this.state = {
            waitingPlayers: [],
            componentUpdated: false,
        };
    }

    closeModal() {
        this.landingComp.setState({nameBoxIsOpen: false, chooseOpponentBoxIsOpen: false});
    }

    componentDidUpdate(prevProps, prevState) {
        if (this.landingComp.state.chooseOpponentBoxIsOpen) {
            clearInterval(window.appInterval);
            window.appInterval = setInterval(() => {
                getWaitingPlayersOrMatchedOpponent(this.landingComp.state.currentPlayer.sessionNumber).then((res) => {
                    if (res.assignedToGame === "true") {
                        this.joinRemoteGame(res.bluePlayer || res.redPlayer, res.gameNumber);
                    }
                    else {
                        this.refreshWaitingPlayers(res, prevState);
                    }

                });
            }, SUSPENSION_TIME);
        }
    }

    refreshWaitingPlayers(res, prevState) {
        let listOfOtherPlayers = res.players.filter(player => player.sessionNumber !== this.landingComp.state.currentPlayer.sessionNumber );
        if (prevState.waitingPlayers.length !== listOfOtherPlayers.length) {
            this.setState({waitingPlayers: listOfOtherPlayers});
        }
    }

    joinRemoteGame(remotePlayer, gameNumber) {
        let sessionStorageObject = JSON.parse(sessionStorage.getItem(SESSION_STORAGE_ITEM_NAME));
        sessionStorageObject.opponentPlayer = remotePlayer;
        sessionStorageObject.currentPlayer.color = getOppositeColor(sessionStorageObject.opponentPlayer.color);
        sessionStorageObject.gameNumber = gameNumber;
        sessionStorage.setItem(SESSION_STORAGE_ITEM_NAME, JSON.stringify(sessionStorageObject));
        clearInterval(window.appInterval);
        this.props.history.push('/two-session-game');
    }

    startRemoteGame(remotePlayer) {
        connectToRemotePlayer(remotePlayer.sessionNumber, this.landingComp.state.currentPlayer.sessionNumber).then((serverData) => {
            let sessionStorageObject = JSON.parse(sessionStorage.getItem(SESSION_STORAGE_ITEM_NAME));
            if (sessionStorageObject.currentPlayer.sessionNumber === serverData.redPlayer.sessionNumber) {
                setPlayersDetailsInSessionStorage(COLOR.RED, serverData.bluePlayer, serverData.gameNumber);
                clearInterval(window.appInterval);
                this.props.history.push('/two-session-game');
            }
            else if (sessionStorageObject.currentPlayer.sessionNumber === serverData.bluePlayer.sessionNumber) {
                setPlayersDetailsInSessionStorage(COLOR.BLUE, serverData.redPlayer, serverData.gameNumber);
                this.props.history.push('/two-session-game');
                clearInterval(window.appInterval);
            }
            else {
                return;
            }

            function setPlayersDetailsInSessionStorage(currentPlayerColor, opponentPlayer, gameNumber) {
                sessionStorageObject.currentPlayer.color = currentPlayerColor;
                sessionStorageObject.opponentPlayer = opponentPlayer;
                sessionStorageObject.opponentPlayer.color = getOppositeColor(currentPlayerColor);
                sessionStorageObject.gameNumber = gameNumber;
                sessionStorage.setItem(SESSION_STORAGE_ITEM_NAME, JSON.stringify(sessionStorageObject));
            }
        });
    }

    render() {
        const customStyles = {
            content : {
                position              : 'fixed',
                display               : 'block',
                top                   : '40%',
                left                  : '50%',
                right                 : 'auto',
                bottom                : '-40%',
                marginRight           : '-50%',
                maxHeight             : '50vh',
                minHeight             : '20vh',
                width                 : '25%',
                transform             : 'translate(-50%, -50%)',
            }
        };

        const elements = this.state.waitingPlayers.map(player => {
                return <button className={styles.opponentBtn} onClick={this.startRemoteGame.bind(this, player)}> {player["playerName"]} </button>
        });
        let message = (elements.length === 0 ? 'We are waiting for other players to register' : 'please choose your opponent');

        return (<Modal isOpen={this.landingComp.state.chooseOpponentBoxIsOpen}
                        style={customStyles} contentLabel="Example Modal">
            <h1 style={{margin: '1vh auto'}}> {this.landingComp.state.currentPlayer.playerName}, </h1>
            <h1 style={{margin: '0vh auto 2vh 2vh', fontWeight: 'normal'}}> {message} </h1>
            <div className={styles.opponentDiv}>
                {elements}
            </div>
        </Modal>);
    }
}

export default withRouter(ChooseOpponentBox);