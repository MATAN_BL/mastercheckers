import React, { Component } from 'react';
import Modal from 'react-modal';
import {registerPlayer, generateUUID} from "../../Utilities/Network";
import styles from './NameBox.css';
import { SESSION_STORAGE_ITEM_NAME, SUSPENSION_TIME } from '../../Utilities/Enum';


export default class NameBox extends Component {
    constructor(props) {
        super(props);
        this.landingComp = this.props.landingComp;
    }

    moveToChooseOpponent() {
        this.registerPlayerAtServer();
        this.landingComp.setState({nameBoxIsOpen: false, chooseOpponentBoxIsOpen: true});
    }

    moveToLandingPage() {
        this.props.history.push("/");
    }

    registerPlayerAtServer() {
        // implement keep-alive method
        let sessionNumber = generateUUID();
        let playerName = document.getElementsByName("Name")[0].value;
        let objectToStore = {
            currentPlayer: {
                sessionNumber: sessionNumber,
                playerName: playerName
            }
        };
        this.landingComp.setState({currentPlayer : objectToStore.currentPlayer});
        sessionStorage.setItem(SESSION_STORAGE_ITEM_NAME, JSON.stringify(objectToStore));
        let interval = setInterval(() => {
            registerPlayer(sessionNumber, playerName)
                .then((res) => {
                    clearInterval(interval);
                    if (res !== null && res !== undefined) {
                        this.sesssionColor = res;
                        this.moveToChooseOpponent();
                    }
                })
        }, SUSPENSION_TIME);
    }

    render() {
        const customStyles = {
            content : {
                top                   : '35%',
                left                  : '50%',
                right                 : 'auto',
                bottom                : 'auto',
                marginRight           : '-50%',
                transform             : 'translate(-50%, -50%)',
                width                 : '25%',
                display               : 'flex',
                justifyContent        : 'center',
                flexDirection         : 'column'
            }
        };

        return (<Modal isOpen={this.landingComp.state.nameBoxIsOpen}
                        style={customStyles} contentLabel="Example Modal">
            <h2 style={{margin: '0 auto 0 auto'}}>Please Enter Your Name</h2>
            <input className = {styles.inputField} type="text" name="Name" />
            <div className={styles.okCancelBtns}>
                <button className={styles.okCancelBtn} onClick={this.moveToChooseOpponent.bind(this)}> OK </button>
                <button className={styles.okCancelBtn} onClick={this.moveToLandingPage.bind(this)}>cancel</button>
            </div>
        </Modal>);
    }
}