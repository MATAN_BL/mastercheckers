import React, { Component } from 'react';
import styles from './Game.css';
import Square from './Square.js';
import { COLOR, CLICK_TYPE, SESSION_STORAGE_ITEM_NAME } from '../../Utilities/Enum.js';
import  MasterPawn from './Pawns/MasterPawn.js';
import BoardUtils from '../../Utilities/BoardUtils.js';
import MessageBox from "./MessageBox.js";
import {readFromServer} from '../../Utilities/Network.js';


class Game extends Component {

    constructor(props) {
        super(props);
        this.boardUtils = new BoardUtils(this);
        this.pawns = this.boardUtils.initBoard(this.props.pawnsIndex);
        this.moveNumber = 1;
        this.state = {
            pawns: this.pawns,
            markedSquares: [],
            turn: COLOR.RED,
            modalMessage: "Welcome! Let's start to play",
            modalIsOpen: true,
            showTurnLabel: true
        };
        this.playingPawn = null;
        this.numberOfPawns = {
            [COLOR.RED]: this.boardUtils.getNumberOfPawnsOfColor(this.pawns, COLOR.RED),
            [COLOR.BLUE]: this.boardUtils.getNumberOfPawnsOfColor(this.pawns, COLOR.BLUE)
        };
    }

    componentWillMount() {
        this.oneSession = this.props.oneSession;
        if (this.oneSession === false) {
            let storedObject = JSON.parse(sessionStorage.getItem(SESSION_STORAGE_ITEM_NAME));
            this.gameNumber = storedObject["gameNumber"];
            this.currentPlayer = storedObject["currentPlayer"];
            this.opponentPlayer = storedObject["opponentPlayer"];
            if (this.currentPlayer.color === COLOR.RED) {
                this.sessionColor = COLOR.RED;
                this.setPlayers(this.currentPlayer, this.opponentPlayer);
            }
            else {
                this.setPlayers(this.opponentPlayer, this.currentPlayer);
                this.sessionColor = COLOR.BLUE;
            }
        }
        else {
            this.redPlayerName = "RED";
            this.bluePlayerName = "BLUE";
        }
    }

    setPlayers(redPlayer, bluePlayer) {
        this.redPlayerName = redPlayer.playerName;
        this.redPlayerMessage = this.redPlayerName + " is RED.";
        this.bluePlayerName = bluePlayer.playerName;
        this.bluePlayerMessage = this.bluePlayerName + " is BLUE.";
    }

    click(row, col) {
        if (this.oneSession === false && this.sessionColor !== this.state.turn) {
            return;
        }
        if (this.playingPawn === null) {
            let clickedPawn = this.getContentOfSquare(row, col);
            if (clickedPawn && clickedPawn.color === this.state.turn) {
                clickedPawn.click(row, col);
            }
        }
        else {
            this.playingPawn.click(row, col);
        }
    }
    
    getContentOfSquare(row, col) {
        return this.state.pawns[row][col];
    }

    movePawn(fromClick, toClick) {
        try {
            let pawnToMove = this.pawns[fromClick.row][fromClick.col];
            if (pawnToMove === null) {
                return;
            }
            this.pawns[fromClick.row][fromClick.col] = null;
            this.pawns[toClick.row][toClick.col] = this.checkCrowning(toClick.row) ?
                (new MasterPawn(this,  pawnToMove.color)) : pawnToMove;
            this.setState({pawns: this.pawns});
        } catch(e) {
            console.error(e);
        }
    }

    checkCrowning(destinatedRow) {
        return (((this.state.turn === COLOR.RED) && (destinatedRow === 0)) ||
        ((this.state.turn === COLOR.BLUE) && (destinatedRow === 7)));
    }

    removePawn(row, col) {
        let pawns = this.state.pawns.slice();
        pawns[row][col] = null;
        this.setState({pawns: pawns});
        this.numberOfPawns[this.boardUtils.opponentTeam()] = this.boardUtils.getNumberOfPawnsOfColor(pawns, this.boardUtils.opponentTeam());
        if (this.numberOfPawns[this.boardUtils.opponentTeam()] === 0) {
            this.setState({modalIsOpen: true, modalMessage: this.state.turn + " wins!", showTurnLabel: false})
        }
    }

    getMarkForSquare(row, col) {
        for (let square of this.state.markedSquares) {
            if (row === square.row && col === square.col) {
               return true;
            }
        }
        return false;
    }

    markSquare(row, col) {
        this.state.markedSquares.push({row: row, col: col});
        this.setState({markedSquares: this.state.markedSquares});
    }

    unmarkAllSquares() {
        this.setState({markedSquares: []});
    }

    renderSquare(row, col) {
        return <Square squareColor={(col + row) % 2 === 0 ? 'white' : 'black'}
                       key={[row, col]} pawn={this.state.pawns[row][col]} row={row} col={col}
                       mark={this.getMarkForSquare(row, col)} onClick={this.click.bind(this, row, col)}/>
    }

    getRow(rowNum) {
        let arr = [];
        for (let i = 0; i < 8; i++) {
            arr.push(this.renderSquare(rowNum, i));
        }
        return (<div key = {rowNum} className={styles.row}> {arr} </div>);
    }


    escFunction(event){
        // once clicking ESC key
        if(event.keyCode === 27) {
            if (this.state.modalIsOpen === true) {
                this.setState({modalIsOpen: false});
            }
            this.setState({markedSquares: []});
            let pawn = this.playingPawn;
            if (pawn) {
                pawn.timeOutForChainMove = null;
                pawn.clickType = CLICK_TYPE.FROM;
                pawn.pawnsToRemove = [];
            }
            this.playingPawn = null;
        }
    }
    componentDidMount(){
        document.addEventListener("keydown", this.escFunction.bind(this), false);
        setInterval(readFromServer.bind(this), 1000);
    }

    render()
    {
        let arr = [];
        for (let i = 0; i < 8; i++) {
            arr.push(this.getRow(i));
        }
        return(
            <div className={styles.app}>
                <MessageBox game={this}/>
                <div>
                    {arr}
                </div>
                {this.state.showTurnLabel ? <div className={styles.panel}>
                    {(this.state.turn === COLOR.RED) ? <h1 className={styles.redFont}> {this.redPlayerName} Turn</h1> :
                        <h1 className={styles.blueFont}> {this.bluePlayerName} Turn </h1>}
                </div> : undefined}
            </div>
        );
    }
}

export default Game;
