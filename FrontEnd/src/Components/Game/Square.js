import React, {Component} from 'react';
import styles from './Square.css';

export default class Square extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        let classesForSquare = [styles[this.props.squareColor]];
        if (this.props.mark) {
            classesForSquare.push(styles.markedSquare);
        }
        if (this.props.pawn) {
            return (<div className={classesForSquare.join(' ')} onClick = {this.props.onClick.bind(this)}>
                <div className={styles[this.props.pawn.getClassName()]}> </div>
            </div>);
        }
        else {
            return <div className={classesForSquare.join(' ')} onClick = {this.props.onClick.bind(this)}>
            </div>
        }
    }
}

