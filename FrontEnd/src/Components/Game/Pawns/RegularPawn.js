import {DIRECTION, COLOR} from '../../../Utilities/Enum.js';
import AbstractPawn from "./AbstractPawn";
import {assert} from '../../../Utilities/Helper.js'


export default class RegularPawn extends AbstractPawn {

    constructor(app, color) {
        super(app, color);
        this.initClicks();
    }

    isBasicMove() {
        if (this.fromClick.row + DIRECTION[this.app.state.turn] !== this.toClick.row) {
            return false;
        }
        if (Math.abs(this.fromClick.col - this.toClick.col) !== 1) {
            return false;
        }
        return true;
    }

    isStrikeMove() {
        let turn = this.app.state.turn;
        assert(turn === COLOR.BLUE || turn === COLOR.RED);
        let direction = ((turn === COLOR.BLUE) ? DIRECTION.BLUE : DIRECTION.RED);
        if ((this.fromClick.row + 2 * direction === this.toClick.row) &&
            (Math.abs(this.fromClick.col - this.toClick.col) === 2) &&
            (this.numberOfOpponentPawnsBetweenMoves(this.fromClick, this.toClick) === 1)) {
            return true;
        }
        return false;
    }

    getClassName() {
        assert(this.color === COLOR.RED || this.color === COLOR.BLUE, "this.color has non-valid value");
        return (this.color === COLOR.RED ? "redPawn" : "bluePawn");
    }
}