import {CLICK_TYPE, COLOR, SUSPENSION_TIME} from '../../../Utilities/Enum.js';
import {writeToServer} from '../../../Utilities/Network.js';
import {getOppositeColor} from '../../../Utilities/Helper.js';


export default class AbstractPawn {

    constructor(app, color) {
        this.app = app;
        this.color = color;
        this.timeOutForChainMove = null;
        this.clickType = CLICK_TYPE.FROM;
        this.pawnsToRemove = [];
        this.initClicks();
    }

    click(row, col) {
        if (this.clickType === CLICK_TYPE.FROM) {
            let squareContent = this.app.getContentOfSquare(row, col);
            if (squareContent.color === this.app.state.turn) {
                this.app.playingPawn = squareContent;
            } else {
                return;
            }
            this.fromClick = {row: row, col: col};
            this.clickType = CLICK_TYPE.TO;
            this.app.markSquare(row, col);
            if (this.isChainMove() === false) {
                this.startMove = {row: row, col: col};
            }
            return;
        }
        if (this.clickType === CLICK_TYPE.TO && this.app.getContentOfSquare(row, col) === null) {
            this.toClick = {row: row, col: col};
            if ( (this.isBasicMove() && !(this.isChainMove())) || this.isStrikeMove()) {
                this.app.markSquare(row, col);
                this.endMove = {row: row, col: col};
                this.fromClick = {row: row, col: col};
                this.clickType = CLICK_TYPE.TO;
                let _this = this;
                clearTimeout(this.timeOutForChainMove);
                this.timeOutForChainMove = null;
                this.timeOutForChainMove = setTimeout(() => {
                    _this.commitMove();
                }, SUSPENSION_TIME);
            } else {
                console.log(`The move from ${JSON.stringify(this.fromClick)} to ${JSON.stringify(this.toClick)} is Illegal`);
            }
        }
    }

    isChainMove() {
        return this.timeOutForChainMove !== null;
    }

    commitMove() {
        this.timeOutForChainMove = null;
        this.app.playingPawn = null;
        this.app.movePawn(this.startMove, this.endMove);
        this.pawnsToRemove.forEach((pawn) => {
            this.app.removePawn(pawn.row, pawn.col);
        });
        let moveObject = {
            gameNumber: this.app.gameNumber,
            moveNumber: this.app.moveNumber,
            fromClick: this.startMove,
            toClick: this.endMove,
            pawnsToRemove: this.pawnsToRemove,
            markedSquares: this.app.state.markedSquares
        };
        this.pawnsToRemove = [];
        this.app.unmarkAllSquares();
        this.initClicks();
        this.app.setState((prevState) => {
            return {turn: getOppositeColor(prevState.turn)}
        });
        this.app.moveNumber++;
        this.clickType = CLICK_TYPE.FROM;
        if (this.app.props.oneSession === false) {
            writeToServer(moveObject).bind(this);
        }
    }

    initClicks() {
        this.toClick = {row: null, col: null};
        this.fromClick = {row: null, col: null};
        this.startMove = {row: null, col: null};
        this.endMove = {row: null, col: null};
    }

    numberOfOpponentPawnsBetweenMoves(fromClick, toClick) {
        let pathLength = Math.abs(fromClick.row - toClick.row);
        let rowDirection = (toClick.row - fromClick.row) / pathLength;
        let colDirection = (toClick.col - fromClick.col) / pathLength;
        let curSquare = JSON.parse(JSON.stringify(fromClick));
        let opponentPawns = [];
        for (let i = 1; i < pathLength; i++) {
            curSquare.row = curSquare.row + rowDirection;
            curSquare.col = curSquare.col + colDirection;
            let pawn = this.app.pawns[curSquare.row][curSquare.col];
            if (pawn && pawn.color === this.app.state.turn) {
                // there is a pawn of your team inside the move
                return -1;
            }
            if (pawn && pawn.color !== this.app.turn) {
                opponentPawns.push({row: curSquare.row, col: curSquare.col});
            }
        }
        let numOfOpponentPawns = opponentPawns.length;
        if (numOfOpponentPawns === 1) {
            this.pawnsToRemove.push({row: opponentPawns[0].row, col: opponentPawns[0].col});
        }
        return numOfOpponentPawns;
    }

    getClassName() {
        return null;
    }

}