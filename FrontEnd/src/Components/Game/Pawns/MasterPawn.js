import {COLOR} from "../../../Utilities/Enum.js";
import {assert} from '../../../Utilities/Helper.js'
import AbstractPawnPlay from "./AbstractPawn.js";
import '../Square.css';

export default class MasterPawn extends AbstractPawnPlay {

    constructor(app, color) {
        super(app, color);
    }

    isBasicMove() {
        if (Math.abs(this.fromClick.col - this.toClick.col) === Math.abs(this.fromClick.row - this.toClick.row) &&
            (this.numberOfOpponentPawnsBetweenMoves(this.fromClick, this.toClick) === 0)) {
            return true;
        }
        return false;
    }

    isStrikeMove() {
        if (Math.abs(this.fromClick.col - this.toClick.col) === Math.abs(this.fromClick.row - this.toClick.row) &&
            (this.numberOfOpponentPawnsBetweenMoves(this.fromClick, this.toClick) === 1)) {
            return true;
        }
        return false;
    }


    getClassName() {
        assert(this.color === COLOR.RED || this.color === COLOR.BLUE, "this.color has non-valid value");
        return (this.color === COLOR.RED ? "redMasterPawn" : "blueMasterPawn");
    }


}
