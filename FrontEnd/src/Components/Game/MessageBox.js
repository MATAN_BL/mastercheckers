import React, { Component } from 'react';
import Modal from 'react-modal';
import styles from "./MessageBox.css";

export default class MessageBox extends Component {
    constructor(props) {
        super(props);
        this.game = this.props.game;
    }

    closeModal() {
        this.game.setState({modalIsOpen: false});
    }

    render() {
        const customStyles = {
            content : {
                top                   : '35%',
                left                  : '45%',
                right                 : 'auto',
                bottom                : 'auto',
                marginRight           : '-50%',
                transform             : 'translate(-50%, -50%)',
                width                 : '30%',
                display               : 'flex',
                justifyContent        : 'center',
                flexDirection         : 'column'
            }
        };

        return (<Modal isOpen={this.game.state.modalIsOpen}
                        style={customStyles} contentLabel="Example Modal">
            <h2 className={styles.messageTitle}>Welcome! Let's start to play</h2>
            <h2 className={[styles.messageTitle, styles.redPlayerMessage].join(' ')}
                style={{display: this.game.oneSession ? 'none' : 'block' }}>
                {this.game.redPlayerMessage}
            </h2>
            <h2 className={[styles.messageTitle, styles.bluePlayerMessage].join(' ')}
                style={{display: this.game.oneSession ? 'none' : 'block' }}>
                {this.game.bluePlayerMessage}
            </h2>
            <button className={styles.messageBoxBtn} onClick={this.closeModal.bind(this)}>GO!</button>
        </Modal>);
    }
}